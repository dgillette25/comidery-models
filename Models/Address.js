export default class Address {
  constructor(data) {
    this.address_1 = data.address_1 ? data.address_1 : "";
    this.address_2 = data.address_2? data.address_2 : "";
    this.city = data.city ? data.city : "";
    this.postal_code = data.postal_code  ? data.postal_code : "";
    this.state = data.state ? data.state : "";
    this.country = data.country ? data.country : "";
    this.first_name = data.first_name ? data.first_name : "";
    this.last_name = data.last_name ? data.last_name : "";
    this.formatted = data.formatted ? data.formatted : "";
    this.phone = data.phone ? data.phone : "";
  }

  exists() {
    return this.id;
  }
  isChef() {
    return this.role.level > 1;
  }
};
