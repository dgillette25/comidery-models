import Meal from "./Meal.js";
import Recipe from "./Recipe.js";

export default class CartLineItem {
  constructor(data) {
    this.id = data.id ? data.id : null;
    this.quantity = data.id ? data.quantity : 0;
    this.meal_id = data.id ? data.meal_id : null;

    this.totalPrice = data.id ? data.totalPrice : 0;
    this.recipe_id = data.id ? data.recipe_id : 0;

    this.recipe = data.id ? new Recipe(data.recipe.data) : {};
    this.meal = data.id ? new Meal(data.meal.data) : {};
  }

  exists() {
    return this.id;
  }
  hasRecipe() {
    return this.recipe;
  }
  hasMeal() {
    return this.meal;
  }
};
