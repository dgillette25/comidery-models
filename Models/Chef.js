import User from "./User.js";
import Address from "./Address.js";

export default class Chef extends User {
  constructor(data) {
    super(data);
    this.address = data.id && data.address ? new Address(data.address.data) : {};
  }
};
