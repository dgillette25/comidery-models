import Recipe from "./Recipe.js";

export default class Meal {
  constructor(data) {
    this.id = data.id ? data.id : null;
    this.completed = data.id ? data.completed : false;
    this.mainImage = data.id ? data.mainImage : null;

    this.cost = data.id ? data.cost : 0;
    this.base_cost = data.id ? data.base_cost : 0;
    this.fees = data.id ? data.fees : 0;
    this.servings = data.id ? data.servings : 0;
    this.servingsOrdered = data.id ? data.servingsOrdered : 0;
    this.service_methods = data.id ? data.service_methods : 0;
    this.distance_in_miles = data.id ? data.distance_in_miles : 0;
    this.til_ready = data.id ? data.til_ready : 0;
    this.rating = data.id ? data.rating : 0;
    this.reviews = data.id ? data.reviews : 0;

    this.recipe = data.id ? new Recipe(data.recipe.data) : {};
    this.user = data.id && data.user ? data.user.data : {};

    this.addons = data.id && data.addons ? data.addons.data : [];
    this.sides = data.id && data.sides ? data.sides.data : [];

    this.window_size = data.id ? data.window_size : 0;
    this.portions = data.id ? data.portions : null;
    this.max_per_window = data.id ? data.max_per_window : 0;
    this.days_available = data.id ? data.days_available : [];
    this.ready_at = data.id ? data.ready_at : null;
    this.completed_at = data.id ? data.completed_at : null;
    this.display_ready_at = data.id ? data.display_ready_at : null;
    this.input_ready_at = data.id ? data.ready_at : null;
    this.display_completed_at = data.id ? data.display_completed_at : null;
    this.input_completed_at = data.id ? data.completed_at : null;
  }

  is() {
    return this.id;
  }
  hasRecipe() {
    return this.recipe;
  }
  availableServings() {
    return this.servings - this.servingsOrdered;
  }
  orderedAddons() {
    return this.addons.filter(a => a.order > 0);
  }
  served() {
    return this.completed_at && (
      new Date(this.completed_at).toUTCString() <
      new Date(Date.now()).toUTCString()
    );
  }
  requestData() {
    return {
      'name': this.name,
      'description': this.description,
      'servings': this.servings,
      'categories': this.categories,
      'images': this.images,
      'ingredients': this.ingredients,
      'recipe_id': this.recipe.id,
      'cost': this.cost,
      'ready_at': this.ready_at,
      'completed_at': this.completed_at,
      'addons': this.addons,
      'service_methods': this.service_methods
    }
  }
};
