import Meal from "./Meal.js";

export default class MealOrder {
  constructor(data) {
    this.id = data.id ? data.id : null;
    this.price = data.id ? data.price : 0;
    this.quantity = data.id ? data.quantity : 0;

    this.meal = data.id ? new Meal(data.meal.data) : {};
    this.order = data.id ? data.order.data : {};
    this.user = data.id ? data.user.data : {};
  }

  exists() {
    return this.id;
  }
};
