export default class User {
  constructor(data) {
    this.id = data.id ? data.id : null;
    this.email = data.id ? data.email : "";
    this.name = data.id ? data.name : "";
    this.following = data.id ? data.following : [];
    this.followers = data.id ? data.followers : [];

    this.position = data.id ? data.position : {};
    this.validated = data.id ? data.validated : false;
    this.role = data.id && data.role ? data.role.data : {};
  }

  exists() {
    return this.id;
  }
  isChef() {
    return this.role !== undefined && this.role.level > 1;
  }
};
