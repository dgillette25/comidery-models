export default class Recipe {
  constructor(data) {
    this.id = data.id ? data.id : null;
    this.name = data.id ? data.name : "";
    this.description = data.id ? data.description : "";
    this.servings = data.id ? data.servings : 1;

    this.categories = data.id && data.categories ? data.categories.data : [];
    this.images = data.id && data.images ? data.images.data : [];
    this.ingredients = data.id && data.ingredients ? data.ingredients.data : [];
    this.nutrients = data.id && data.nutrients ? data.nutrients.data : [];
    this.followers = data.id ? data.followers : 0;
    this.mainImage = data.id ? data.mainImage : "";
    this.rating = data.id ? data.rating : 0;
    this.reviews = data.id ? data.reviews : 0;

    this.nutritionalFacts = data.id ? data.nutritionalFacts : {showable: [], stats: []};
    this.user = data.id && data.user ? data.user.data : {};
  }

  exists() {
    return this.id;
  }
  hasImages() {
    return this.getImages().length;
  }
  getImages() {
    return this.images;
  }
  getFormattedImages(root_path) {
    return this.getImages().map(
      image => root_path + '/' + image.path
    );
  }
};
